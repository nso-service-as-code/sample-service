# -*- mode: python; python-indent: 4 -*-
import ncs
from ncs.application import Service


# ------------------------
# SERVICE CALLBACK EXAMPLE
# ------------------------
class ServiceCallbacks(Service):

    # The create() callback is invoked inside NCS FASTMAP and
    # must always exist.
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info('Service create(service=', service._path, ')')

        vars = ncs.template.Variables()
        template = ncs.template.Template(service)

        ad = service.access_device
        dd = service.delivery_device
        link = service.link

        qos_policy_name = service.link.qos_policy_name

        # FILL CONFIGURATION TEMPLATE FOR ACCESS DEVICE

        # iface description is generated using multiple input parameters define the yang instance
        iface_description = "-"
        iface_d_params = ("service", service.name, "svc", "to", dd.device_id, dd.interface_id, str(dd.vlan_id))
        iface_description = iface_description.join(iface_d_params)

        vars.add('DEVICE_NAME', ad.device_id)
        vars.add('INTERFACE_ID', ad.interface_id)
        vars.add('SERVICE_ETHERNET_INSTANCE_ID', ad.instance_id)
        vars.add('QOS_POLICY_NAME', service.link.qos_policy_name)
        vars.add('VLAN_ID', ad.vlan_id)
        vars.add('REMOTE_IP', dd.ip_address)
        vars.add('VC_ID',link.vc_id)
        vars.add('VC_CLASS',link.vc_class)
        vars.add('IFACE_DESCRIPTION', iface_description)

        # after collecting all required parameters,
        # they are applied in the l2vpn-template to the candidate configuration of NSO
        template.apply('sample-template', vars)

        # FILL CONFIGURATION TEMPLATE FOR DELIVERY DEVICE

        iface_description = "-"
        iface_d_params = ("service", service.name, "svc", "to", ad.device_id, ad.interface_id, str(ad.vlan_id))
        iface_description = iface_description.join(iface_d_params)

        vars.add('DEVICE_NAME', dd.device_id)
        vars.add('INTERFACE_ID', dd.interface_id)
        vars.add('SERVICE_ETHERNET_INSTANCE_ID', dd.instance_id)
        vars.add('QOS_POLICY_NAME', qos_policy_name)
        vars.add('VLAN_ID', dd.vlan_id)
        vars.add('REMOTE_IP', ad.ip_address)
        vars.add('IFACE_DESCRIPTION', iface_description)

        template.apply('sample-template', vars)

# ---------------------------------------------
# COMPONENT THREAD THAT WILL BE STARTED BY NCS.
# ---------------------------------------------
class Main(ncs.application.Application):
    def setup(self):
        self.log.info('Main RUNNING')
        self.register_service('sample-servicepoint', ServiceCallbacks)

    def teardown(self):
        self.log.info('Main FINISHED')
