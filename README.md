# NSO Sample Service

This repo is used to build a docker image of NSO containing a simple service used for NSO Service as Code concept presentation

## Folder structure

* config             - NSO conficuration files, such as authgroups.xml
* ned-packages       - NEDs to be included in the image
* nso-install-files  - NSO installation file
* packages           - Service packagees to be included in the image

## Image build procedure

1. Clone the repository
2. Download NSO installation binaries into nso-install-files folder
3. Download NED packages into ned-packages folder. In this case we only need cisco-ios NED
4. Compile package
5. Build and push into gitlab registry

```
docker login registry.gitlab.com
docker build --build-arg PACKAGES_DIR=ned-packages --build-arg NSO_INSTALL_FILE=nso-install-files/nso-6.1.linux.x86_64.installer.bin -t registry.gitlab.com/nso-service-as-code/sample-service nso/.
docker push registry.gitlab.com/nso-service-as-code/sample-service
```